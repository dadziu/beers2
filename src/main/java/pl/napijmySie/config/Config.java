package pl.napijmySie.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import pl.napijmySie.api.IBeer;
import pl.napijmySie.implementation.Beer;

import java.util.ArrayList;
import java.util.List;


@Configuration
public class Config {

    @Bean
    public List<IBeer> beers(){ //(2)
        List<IBeer> beers = new ArrayList<>();
        beers.add(new Beer("Warka", 4.5 , 3.20));
        beers.add(new Beer("Tyskie", 5.2, 4.10));
        beers.add(new Beer("Lech", 5.5, 3.99));
        beers.add(new Beer("Zywiec", 5.8, 3.70));
        beers.add(new Beer("Ciechan", 6.2, 5.30));
        return beers;
    }
}
