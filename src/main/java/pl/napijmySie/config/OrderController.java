package pl.napijmySie.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import pl.napijmySie.api.IBeer;

import java.util.List;


@Component
@Controller
public class OrderController {

    @Value("#{beers[3]}") //(2) takes value from List of 'beers()' method (from Bean)
    private IBeer beer;


    public OrderController(List<IBeer> beer) {
        super();
    }//(1) this class will take list of beers as parameter from Beans

    public String showOrder() {
        return ("nazwa: " + beer.getName() +
                "procenty: " + beer.getAlcoholPercentage() +
                "cena: " + beer.getPrice() + "PLN");
    }

    @GetMapping(value = "/")
    public String piwka(ModelMap model) {
        //model.addAttribute("stats", showOrder());
        model.addAttribute("name", beer.getName()); //injects to .jsp file
        model.addAttribute("alcoholPercentage", beer.getAlcoholPercentage() + "%");
        model.addAttribute("price", beer.getPrice() + "PLN");
        return "piwka";
    }
}
