<%@page contentType = "text/html;charset = UTF-8" language = "java" %>
<%@page isELIgnored = "false" %>
<%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form"%>
<html>
<head>
    <title>Napijmy sie - bar dla 'ambitnych'</title>
</head>
<body>
<h1>Napijmy sie - bar dla 'ambitnych'</h1>
<h1>MENU:</h1>
<table>
<tr>
    <td>${name}</td>
</tr>
<tr>
    <td>Procenty:</td>
    <td>${alcoholPercentage}</td>
</tr>
<tr>
    <td>Cena:</td>
    <td>${price}</td>
</tr>
</table>
</body>
</html>